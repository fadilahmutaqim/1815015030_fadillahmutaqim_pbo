package Bioskop;

public class film {
    protected String Nama_film;
    protected String Nama_Pembeli;
    protected int tiket;
    protected String Jam;
    protected String Hari;
    
    public void desc(){
        System.out.println("Pemesanan Tiket");
    }

    public String getNama_film() {
        return Nama_film;
    }

    public void setNama_film(String Nama_film) {
        this.Nama_film = Nama_film;
    }

    public String getNama_Pembeli() {
        return Nama_Pembeli;
    }

    public void setNama_Pembeli(String Nama_Pembeli) {
        this.Nama_Pembeli = Nama_Pembeli;
    }

    public int getTiket() {
        return tiket;
    }

    public void setTiket(int tiket) {
        this.tiket = tiket;
    }

    public String getJam() {
        return Jam;
    }

    public void setJam(String Jam) {
        this.Jam = Jam;
    }

    public String getHari() {
        return Hari;
    }

    public void setHari(String Hari) {
        this.Hari = Hari;
    }

  

}
